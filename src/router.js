import Vue from 'vue'
import Router from 'vue-router'
import Company from './views/company/Company'
import Home from './views/Home.vue'
import About from './views/About'
import Login from './views/login/Login'
import Dashboard from './views/Dashboard'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    /* { 
      path:
      name: 'noMatch',
      component: Home
    }, */
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/dashboard',
      name: 'dashboard',
     /*  component: () => import( *//* webpackChunkName: "dashboard" */ /* './views/Dashboard.vue'), */
      component: Dashboard,
      children: [
        {
          path: 'company',
          component: Company
        },
        {
          path: 'employee',
          component: Home
        },
        {
          path: 'user',
          component: About
        },
        {
          path: 'role',
          component: About
        },
        {
          path: 'souvenir',
          component: About
        },
      ]
    },
    
  ]
})
