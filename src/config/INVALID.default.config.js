module.exports = {
    BASE_URL : 'http://localhost:3000/api',
    ROUTE : {
        COMPANY: '/company',
        LOGIN : '/auth/login',
    },
    LOCAL : {
        USERDATA : 'SECRET_USERDATA_FA0810',
        TOKEN : 'SECRET_TOKEN_FA0810'
    } 
}