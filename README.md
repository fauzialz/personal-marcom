
# vue-project-2

# Development Note

I build this app by using [Vue](https://vuejs.org/), start it with [Vue_CLI_3](https://cli.vuejs.org/) and adding some stuff in the process. For faster development purpose I add [Vuetify](https://vuetifyjs.com/en/) as a component framework that applies [Material_Design](https://material.io/design/) style from Google.

This app using webpack, the module working under the hood in vue/cli-service.

If you curious to learn how I set up everything and getting started please be eager to look at his video created by [Academind](https://www.youtube.com/watch?v=nSmMkeNjjPg&t=733s) that I use as a reference. It might help you.

After this you will learn how to run the app.

# Guide Note

### Important Summary

As it run, this project will connect to [API](https://gitlab.com/fauzial/personal-marcom-api) that up until now sill use local MongoDB. So, please follow the instructions in API link because you can't access this app without Login. This app also still doesn't have the mechanism to register new user from Login UI level. So please register new user first by post new username and password using Postman to `localhost:3000/api/user/`.

After you successfully Login, if the API is running, you will find the data table is empty. Hit plus button to create new one. In case the API is not running, the UI will show retry to connect mechanism.

### Project setup

Install [npm](https://nodejs.org/en/download/) first, then you must do:
```
npm install
```
to install all the required modules/

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
